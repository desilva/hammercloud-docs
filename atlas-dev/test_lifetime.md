# Test lifetime

Test lifetime consists of 3 phases: 

  1. **Test generation:** from admin template and .tpl file the job configuration files (files jobs/*.py) are created.
  2. **Test submission:** first bunch of jobs from configuration files are submitted to PanDA.
  3. **Monitoring loop:** monitoring loop keeps an eye on submitted/running jobs, and makes sure that desired number of jobs is running at each site (and submits more jobs once the running ones complete). At the end of the test request to cancel non-complete submitted/running jobs is issued. 




    