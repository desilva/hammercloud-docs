Massive blacklisting mitigation
===


# Disable hammercloud probe in AGIS controller
* On Monday 6th Feb 2017 the PanDA controller was decommissioned, and succeeded by AGIS controller: [elog:60136](https://atlas-logbook.cern.ch/elog/ATLAS+Computer+Operations+Logbook/60136).
* In the event of massive blacklisting that should not be happening (e.g. mis-configured DBrelease on cvmfs causes failure of all PFT jobs hence massive blacklisting of all production resources despite other jobs that do not use DBrelease are running happily), ATLAS HC Ops team needs to disable hammercloud probe in AGIS.
    * ATLAS HC Ops team can be reached through e-mail to <atlas-adc-hammercloud-support@cern.ch>.
* How to disable hammercloud probe in AGIS:
    * Visit [https://atlas-agis.cern.ch/agis/probe/edit/hammercloud/](https://atlas-agis.cern.ch/agis/probe/edit/hammercloud/)
    * Set ``State`` to ``DISABLED``.
    * Click on button ``Check input data``, you'll get to the new page with validation form (header: ``Please confirm the changes to be submitted for object hammercloud``).
     * On the validation form page, review intended changes and confirm with button ``Save & continue``.
        * If you did not change status of the ``hammercloud`` probe, the save button will not be available. 
     * Do not forget to re-enable the ``hammercloud`` probe in AGIS once the cause for massive blacklisting is fixed! Communicate this with ATLAS CRC shifter.
      * **N.B.:** if you do not have probe editing **privileges** in AGIS and think you should possess them, register your DN in AGIS: [https://atlas-agis.cern.ch/agis/user/register/dn](https://atlas-agis.cern.ch/agis/user/register/dn) and ask the AGIS team to be added to the ``ATLAS-Blacklisting`` user group. 
     * If a bunch of queues was blacklisted but should not have been, and the ``hammercloud`` probe is disabled in AGIS, the blacklisted queues have to be reviewed and un-blacklisted.



# Revert blacklisting
* If a bunch of queues was blacklisted but should not have been, and the ``hammercloud`` probe is disabled in AGIS, the blacklisted queues have to be reviewed and un-blacklisted.
    * This page contains list of blacklisting/whitelisting events in the past 24 hrs. You can filter there for pattern of job failures, to distinguish between queues that were not blacklisted on accident, but rightfully, and queues that were victim to unfortunate massive blacklisting: [http://hammercloud.cern.ch/hc/app/atlas/robot/blacklisting/](http://hammercloud.cern.ch/hc/app/atlas/robot/blacklisting/)
     * AGIS provides documentation what commands to run to change status of PanDA queues: http://atlas-agis.cern.ch/agis/pandablacklisting/list/ 

* The ATLAS HC Ops  team can revert blacklisting by running a script on any of the ATLAS HC nodes: 

```
### e.g. from [root@hammercloud-ai-12 ~]# 

# STARTDATE=2017-10-03+07:20
# ENDDATE=2017-10-03+08:00
# /data/hc/apps/atlas/scripts/server/revert-blacklisting.sh ${STARTDATE} ${ENDDATE}  2>&1 | tee log.revert_blacklisting.`date +%F.%H%M%S`.log

```


