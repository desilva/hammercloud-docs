# Self-approve a test
A HC user is able to self-approve a test, when the account belongs to a `self_approver` group, e.g. `atlas_self_approver` or `cms_self_approver`. 

## Howto
1. Login to the HammerCloud administration page, e.g. [https://hammercloud.cern.ch/hc/admin/atlas/](https://hammercloud.cern.ch/hc/admin/atlas/) or [https://hammercloud.cern.ch/hc/admin/cms/](https://hammercloud.cern.ch/hc/admin/cms/).
2. Go to test admin, [https://hammercloud.cern.ch/hc/admin/atlas/test/](https://hammercloud.cern.ch/hc/admin/atlas/test/) or [https://hammercloud.cern.ch/hc/admin/cms/test/](https://hammercloud.cern.ch/hc/admin/cms/test/).
3. Check box of the line with your desired test.
4. In the _Action_ menu select option `Mark selected test as approved`.
5. Click on `Go` button of the _Action_ menu. 
   * Test should change its status from `draft` to `tobescheduled`. 
6. In case of troubles, contact HammerCloud team.